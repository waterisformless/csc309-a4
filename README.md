A web-based version of the classic 70's game Connect 4. Developed as part of [CSC309H1F Fall 2014 assignment 4](http://www.cs.toronto.edu/~delara/courses/csc309/) at the University of Toronto. 

# Technologies Used #
CodeIgniter, AJAX, JSON, JQuery, CSS and MySQL

# Game Rules #
The game is played by two players which take turns dropping colored discs from the top into a seven-column, six-row vertically suspended grid. The pieces fall straight down, occupying the next available space within the column. The first player that connects four discs of the same color next to each other vertically, horizontally, or diagonally wins!