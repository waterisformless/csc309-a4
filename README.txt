## CAPTCHA
CAPTCHAs are implemented using the securimage PHP library. The default CAPTCHA check script provided on the site is used
for validating the input text.