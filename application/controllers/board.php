<?php

class Board extends CI_Controller {
     
    function __construct() {
    		// Call the Controller constructor
	    	parent::__construct();
	    	session_start();
    } 
          
    public function _remap($method, $params = array()) {
	    	// enforce access control to protected functions	
    		
    		if (!isset($_SESSION['user']))
   			redirect('account/loginForm', 'refresh'); //Then we redirect to the index page again
 	    	
	    	return call_user_func_array(array($this, $method), $params);
    }
    
    
    function index() {
		$user = $_SESSION['user'];
    		    	
	    	$this->load->model('user_model');
	    	$this->load->model('invite_model');
	    	$this->load->model('match_model');
	    	
	    	$user = $this->user_model->get($user->login);

	    	$invite = $this->invite_model->get($user->invite_id);
	    	
	    	if ($user->user_status_id == User::WAITING) {
	    		$invite = $this->invite_model->get($user->invite_id);
	    		$otherUser = $this->user_model->getFromId($invite->user2_id);
	    	}
	    	else if ($user->user_status_id == User::PLAYING) {
	    		$match = $this->match_model->get($user->match_id);
	    		if ($match->user1_id == $user->id)
	    			$otherUser = $this->user_model->getFromId($match->user2_id);
	    		else
	    			$otherUser = $this->user_model->getFromId($match->user1_id);
	    	}
	    	
	    	$data['user']=$user;
	    	$data['otherUser']=$otherUser;
	    	
	    	switch($user->user_status_id) {
	    		case User::PLAYING:	
	    			$data['status'] = 'playing';
	    			break;
	    		case User::WAITING:
	    			$data['status'] = 'waiting';
	    			break;
	    	}
	    	
		$this->load->view('match/board',$data);
    }

 	function postMsg() {
 		$this->load->library('form_validation');
 		$this->form_validation->set_rules('msg', 'Message', 'required');
 		
 		if ($this->form_validation->run() == TRUE) {
 			$this->load->model('user_model');
 			$this->load->model('match_model');

 			$user = $_SESSION['user'];
 			 
 			$user = $this->user_model->getExclusive($user->login);
 			if ($user->user_status_id != User::PLAYING) {	
				$errormsg="Not in PLAYING state";
 				goto error;
 			}
 			
 			$match = $this->match_model->get($user->match_id);			
 			
 			$msg = $this->input->post('msg');
 			
 			if ($match->user1_id == $user->id)  {
 				$msg = $match->u1_msg == ''? $msg :  $match->u1_msg . "\n" . $msg;
 				$this->match_model->updateMsgU1($match->id, $msg);
 			}
 			else {
 				$msg = $match->u2_msg == ''? $msg :  $match->u2_msg . "\n" . $msg;
 				$this->match_model->updateMsgU2($match->id, $msg);
 			}
 				
 			echo json_encode(array('status'=>'success'));
 			 
 			return;
 		}
		
 		$errormsg="Missing argument";
 		
		error:
			echo json_encode(array('status'=>'failure','message'=>$errormsg));
 	}
 
	function getMsg() {
 		$this->load->model('user_model');
 		$this->load->model('match_model');
 			
 		$user = $_SESSION['user'];
 		 
 		$user = $this->user_model->get($user->login);
 		if ($user->user_status_id != User::PLAYING) {	
 			$errormsg="Not in PLAYING state";
 			goto error;
 		}
 		// start transactional mode  
 		$this->db->trans_begin();
 			
 		$match = $this->match_model->getExclusive($user->match_id);			
 			
 		if ($match->user1_id == $user->id) {
			$msg = $match->u2_msg;
 			$this->match_model->updateMsgU2($match->id,"");
 		}
 		else {
 			$msg = $match->u1_msg;
 			$this->match_model->updateMsgU1($match->id,"");
 		}

 		if ($this->db->trans_status() === FALSE) {
 			$errormsg = "Transaction error";
 			goto transactionerror;
 		}
 		
 		// if all went well commit changes
 		$this->db->trans_commit();
 		
 		echo json_encode(array('status'=>'success','message'=>$msg));
		return;
		
		transactionerror:
		$this->db->trans_rollback();
		
		error:
		echo json_encode(array('status'=>'failure','message'=>$errormsg));
 	}
 	
 	function getState() {
 		$this->load->model('user_model');
 		$this->load->model('match_model');
 	
 		$user = $_SESSION['user'];
 	
 		$user = $this->user_model->get($user->login);
 		if ($user->user_status_id != User::PLAYING) {
 			$errormsg="Not in PLAYING state";
 			goto error;
 		}
 		// start transactional mode
 		$this->db->trans_begin();
 	
 		$match = $this->match_model->getExclusive($user->match_id);
 	
 		$state = json_decode($match->board_state);
 	
 		if ($this->db->trans_status() === FALSE) {
 			$errormsg = "Transaction error";
 			goto transactionerror;
 		}
 	
 		// if all went well commit changes
 		$this->db->trans_commit();
 	
 		//echo json_encode(array('status'=>'success','state'=>$state));
 		echo json_encode(array('status'=>'success','state'=>$state, 'win'=>$match->match_status_id));
 		return;
 	
 		transactionerror:
 		$this->db->trans_rollback();
 	
 		error:
 		echo json_encode(array('status'=>'failure','state'=>$errormsg));
 	}
 	
 	function postState() {
 		$this->load->library('form_validation');
 		$this->form_validation->set_rules('coord', 'Coordinate', 'required');
 			
 		if ($this->form_validation->run() == TRUE) {
 			$this->load->model('user_model');
 			$this->load->model('match_model');
 	
 			$user = $_SESSION['user'];
 				
 			$user = $this->user_model->getExclusive($user->login);
 			if ($user->user_status_id != User::PLAYING) {
 				$errormsg="Not in PLAYING state";
 				goto error;
 			}
 			
 			$match = $this->match_model->get($user->match_id);
 			
 			$state = json_decode($match->board_state);

 			$empty = 0;
 			
 			foreach ($state as $row){
 				foreach ($row as $column){
					if ($column == '&nbsp;'){
						$empty += 1;
					} 					
 				}
 			}

            $match_status = 1;

            $last_row = 0;
            $last_col = 0;

            // prevent invalid (out-of-turn) moves
 			if (($empty % 2 == 1 && $match->user1_id == $user->id) || ($empty % 2 == 0 && $match->user2_id == $user->id)){
 			
	 			$coord = $this->input->post('coord');
	 			
	 			for ($i = 5; $i >= 0; $i--){
	 				if ($state[$i][$coord] == '&nbsp;'){
	 					if ($user->id == $match->user1_id) {
	 						$state[$i][$coord]='X';
                            $marker='X';
                            $match_status = 2;
	 					}
	 					else {
	 						$state[$i][$coord]='O';
                            $marker='O';
                            $match_status = 3;
	 					}
	                    $last_row = $i;
	                    $last_col = $coord;
	 					break;
	 				}
	 			}
	 		}

            $empty = 0;

            foreach ($state as $row){
                foreach ($row as $column){
                    if ($column == '&nbsp;'){
                        $empty += 1;
                    }
                }
            }
            // put update in transaction
            $this->db->trans_begin();
 			$this->match_model->updateState($match->id, json_encode($state));

            // check if winning move and update match status accordingly
            if ($this->checkForWinner($last_row, $last_col, $marker, $state)){
                $this->match_model->updateStatus($user->match_id, $match_status);
            }
            elseif ($empty == 0){
                $this->match_model->updateStatus($user->match_id, 4);
            }

            if ($this->db->trans_status() === FALSE) {
                $errormsg = "Transaction error";
                goto transactionerror;
            }
            // if all went well commit changes
            $this->db->trans_commit();
 			 				
 			echo json_encode(array('status'=>'success'));
 			
 			return;
 		}
 	
 		$errormsg="Missing argument";

        transactionerror:
        $this->db->trans_rollback();
 			
 		error:
 		echo json_encode(array('status'=>'failure','message'=>$errormsg));
 	}

    function checkForWinner($last_row, $last_col, $marker, $state){

        $row = $last_row;
        $col = $last_col;

        // diagonals
        $score_before = 0;
        $score_after = 0;
        for ($i = 1; $i < 4; $i++){
            if ($row + $i <= 5 && $col - $i >= 0) {
                if ($score_before < 3 && $state[$row + $i][$col - $i] == $marker) {
                    $score_before++;
                    if ($score_before == 3) {
                        goto set_winner;
                    }
                } else {
                    break;
                }
            }
        }
        for ($i = 1; $i < 4; $i++){
            if ($row - $i >= 0 && $col + $i <= 6) {
                if ($score_after < 3 && $state[$row - $i][$col + $i] == $marker) {
                    $score_after++;
                    if ($score_after + $score_before == 3) {
                        goto set_winner;
                    }
                } else {
                    break;
                }
            }
        }

        // antidiagonals
        $score_before = 0;
        $score_after = 0;
        for ($i = 1; $i < 4; $i++){
            if ($row + $i <= 5 && $col + $i <= 6) {
                if ($score_before < 3 && $state[$row + $i][$col + $i] == $marker) {
                    $score_before++;
                    if ($score_before == 3) {
                        goto set_winner;
                    }
                } else {
                    break;
                }
            }
        }
        for ($i = 1; $i < 4; $i++){
            if ($row - $i >= 0 && $col - $i >= 0) {
                if ($score_after < 3 && $state[$row - $i][$col - $i] == $marker) {
                    $score_after++;
                    if ($score_after + $score_before == 3) {
                        goto set_winner;
                    }
                } else {
                    break;
                }
            }
        }
        // column
        $score_before = 0;
        $score_after = 0;
        for ($i = 1; $i < 4; $i++){
            if ($col - $i >= 0) {
                if ($score_before < 3 && $state[$row][$col - $i] == $marker) {
                    $score_before++;
                    if ($score_before == 3) {
                        goto set_winner;
                    }
                } else {
                    break;
                }
            }
        }
        for ($i = 1; $i < 4; $i++){
            if ($col + $i <= 6) {
                if ($score_after < 3 && $state[$row][$col + $i] == $marker) {
                    $score_after++;
                    if ($score_after + $score_before == 3) {
                        goto set_winner;
                    }
                } else {
                    break;
                }
            }
        }

        // row
        $score_before = 0;
        $score_after = 0;
        for ($i = 1; $i < 4; $i++){
            if ($row - $i >= 0) {
                if ($score_before < 3 && $state[$row - $i][$col] == $marker) {
                    $score_before++;
                    if ($score_before == 3) {
                        goto set_winner;
                    }
                } else {
                    break;
                }
            }
        }
        for ($i = 1; $i < 4; $i++){
            if ($row + $i <= 5) {
                if ($score_after < 3 && $state[$row + $i][$col] == $marker) {
                    $score_after++;
                    if ($score_after + $score_before == 3) {
                        goto set_winner;
                    }
                } else {
                    break;
                }
            }
        }
        return false;

        set_winner:
        return true;
    }
 }

