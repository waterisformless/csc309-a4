
<!DOCTYPE html>

<html>
	<head>
	<script src="http://code.jquery.com/jquery-latest.js"></script>
	<script src="<?= base_url() ?>/js/jquery.timers.js"></script>
	<script>

		var otherUser = "<?= $otherUser->login ?>";
		var user = "<?= $user->login ?>";
		var status = "<?= $status ?>";
		
		$(function(){
			$('body').everyTime(500,function(){
				if (status == 'waiting') {
					$.getJSON('<?= base_url() ?>arcade/checkInvitation',function(data, text, jqZHR){
							if (data && data.status=='rejected') {
								alert("Sorry, your invitation to play was declined!");
								window.location.href = '<?= base_url() ?>arcade/index';
							}
							if (data && data.status=='accepted') {
								status = 'playing';
								$('#status').html('Playing ' + otherUser);
							}
							
					});
				}
				var url = "<?= base_url() ?>board/getMsg";
				$.getJSON(url, function (data,text,jqXHR){
					if (data && data.status=='success') {
						var conversation = $('[name=conversation]').val();
						var msg = data.message;
						if (msg.length > 0)
							$('[name=conversation]').val(conversation + "\n" + otherUser + ": " + msg);
					}
				});
				var url = "<?= base_url() ?>board/getState";
				$.getJSON(url, function (data,text,jqXHR) {
					if (data && data.status=='success') {
						if (data.win == 2){
							alert("Player 2 wins!");
							window.location.href = '<?= base_url() ?>arcade/index';
						}
						if (data.win == 3){
							alert("Player 1 wins!");
							window.location.href = '<?= base_url() ?>arcade/index';
						}
						if (data.win == 4){
							alert("It's a tie!");
							window.location.href = '<?= base_url() ?>arcade/index';
						}
						var state = data.state;
						var htmlBoard = '<table border = "1">';
						
						for (var i = 0; i < state.length; i++) { 
							htmlBoard += "<tr>";
							for (var j = 0; j < state[i].length; j++) {
								htmlBoard += '<td class="c4cell" id ="' + j + '" style=>';
                                if (state[i][j] == 'X'){
                                    htmlBoard += '<img src="<?= base_url() ?>/images/c4red.png" height="40" width="40">';
                                }
                                else if(state[i][j] == 'O'){
                                    htmlBoard += '<img src="<?= base_url() ?>/images/c4yellow.png" height="40" width="40">';
                                }
                                else{
                                    htmlBoard += '<img class="blank" src="" height="40" width="40">';
                                }
                                htmlBoard += "</td>";
							}
							htmlBoard += "</tr>";
						}

						htmlBoard += "</table>";
						document.getElementById("board_state").innerHTML = htmlBoard;
					}
                    $('td.c4cell').css("border-radius","90%");
                    $('img.blank').css("visibility","hidden");
				});
			});

			$(document).on("click", "td", function(event){
				var arguments = "coord=" + $(this).attr("id");
				var url = "<?= base_url() ?>board/postState";
				$.post(url,arguments, function (data,textStatus,jqXHR){	
				});
				return false;
			});

			$('form').submit(function(){
				var arguments = $(this).serialize();
				var url = "<?= base_url() ?>board/postMsg";
				$.post(url,arguments, function (data,textStatus,jqXHR){
						var conversation = $('[name=conversation]').val();
						var msg = $('[name=msg]').val();
						$('[name=conversation]').val(conversation + "\n" + user + ": " + msg);
						});
				return false;
			});	
		});
	
	</script>
	</head> 
<body>  
	<h1>Game Area</h1>

	<div>
	Hello <?= $user->fullName() ?>  <?= anchor('account/logout','(Logout)') ?>  
	</div>
	
	<div id='status'> 
	<?php 
		if ($status == "playing")
			echo "Playing " . $otherUser->login;
		else
			echo "Wating on " . $otherUser->login;
	?>
	</div>
	
<?php 
	
	echo '<div id = "board_state" style = "-webkit-user-select: none; -moz-user-select: none; font-family: Monaco, monospace;"></div>';


	echo form_textarea('conversation');
	
	echo form_open();
	echo form_input('msg');
	echo form_submit('Send','Send');
	echo form_close();
	
?>
	
	
	
	
</body>

</html>

